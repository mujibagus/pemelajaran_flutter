import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    TabBar myTabBar = TabBar(
      indicator: BoxDecoration(
          color: Colors.red,
          border: Border(
              bottom: BorderSide(
                  color: Colors
                      .blueAccent, width: 5))), // indicator dan box decoration unutk memblok tammpilan tab bar
      //indicatorColor: Colors.blueAccent,// indicaatorColor adalah cara mengubah indikator warna
      tabs: <Widget>[
        Tab(icon: Icon(Icons.comment), text: "Comments"),
        Tab(
          child: Image(
            image: AssetImage("assets/cute.png"),
          ),
        ),
        Tab(
          icon: Icon(Icons.computer),
        ),
        Tab(
          text: "News",
        )
      ],
    );

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 4, // menambahkan berapa jumlah tab yang akan dibuat
        child: Scaffold(
          appBar: AppBar(
              title: Text("Contoh Tab Bar"),
              bottom: PreferredSize(
                  // preferredSize untuk mengubah tampilan tab
                  preferredSize: Size.fromHeight(myTabBar.preferredSize.height),
                  child: Container(color: Colors.amber, child: myTabBar))),
          body: TabBarView(
            children: <Widget>[
              Center(
                child: Text("Tab 1"),
              ),
              Center(
                child: Text("Tab 2"),
              ),
              Center(
                child: Text("Tab 3"),
              ),
              Center(
                child: Text("Tab 4"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
