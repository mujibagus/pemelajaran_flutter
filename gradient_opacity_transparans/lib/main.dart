

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Gradient Opacity"),
        ),
        body: Center(
          child: ShaderMask(
            // membuat opasiti / transparan menggunakan shader mask
            shaderCallback: (Rectangle) {
              return LinearGradient(
                colors: [Colors.black, Colors.transparent],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              ).createShader(Rect.fromLTRB(0, 0, Rectangle.width, Rectangle.height));

            },
            blendMode: BlendMode.dstIn,
            child: Image(
              width: 300,
              image: NetworkImage(
                  "https://pngimage.net/wp-content/uploads/2018/06/gambar-pemandangan-png-5.png"),
            ),
          ),
        ),
      ),
    );
  }
}
