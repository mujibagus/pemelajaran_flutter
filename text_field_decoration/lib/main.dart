import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  TextEditingController controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Latihan Text Field"),
        ),
        body: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              TextField(
                decoration: InputDecoration(
                  fillColor: Colors.lightBlue[50],
                  filled:true,
                    icon: Icon(Icons.adb),
                    prefixIcon: Icon(Icons.person),
                    prefixText: "Name :",
                    prefixStyle: TextStyle(color: Colors.blue, fontWeight: FontWeight.w500),
                    labelText: "Name lengkap",
                    hintText: "Nama lengkap lo ", // untuk memberitahu text apa yang harus ditulis
                    border:
                        OutlineInputBorder(borderRadius: BorderRadius.circular(15)) // border diluar "OutlineInputBorder()" // untuk menghilangkan border "InputBorder.none"
                    ),
                    maxLength: 25,
                // obscureText: true, // untuk password jadi titik2
                onChanged: (value) {
                  setState(() {});
                },
                controller: controller,
              ),
              Text(controller.text)
            ],
          ),
        ),
      ),
    );
  }
}
