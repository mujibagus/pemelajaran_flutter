import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Center(
          child: QrImage(
            version: QrVersions.auto,
            backgroundColor: Colors.transparent,
            foregroundColor: Colors.red,
            errorCorrectionLevel: QrErrorCorrectLevel.H,
            padding: EdgeInsets.all(2),
            size: 300,
            data: 'This is a simple QR code Muji Bagus T',
            gapless: false,
          ),
        ),
      ),
    );
  }
}