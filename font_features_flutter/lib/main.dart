import 'dart:ui';

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp( debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("Flutter typographi"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Text("Contoh 01 ( tanpa apapun )",
                  style: TextStyle(fontSize: 20)),
              Text("Contoh 02 ( Small Caps )",
                  style: TextStyle(
                      fontSize: 20,
                      fontFeatures: [FontFeature.enable('smcp')])),
              Text("Contoh 03 ( Small Caps 3 1/2)",
                  style: TextStyle(fontSize: 20, fontFeatures: [
                    FontFeature.enable('smcp'),
                    FontFeature.enable('frac')
                  ])),
              Text("Contoh milonga 04 ( Small Caps 3 1/2)",
                  style: TextStyle(fontSize: 20, fontFamily: "Milonga", fontFeatures: [
                    FontFeature.enable('smcp'),
                    FontFeature.enable('frac')
                  ])),
              Text("Contoh cardo 219( tanpa apapun)",
                  style: TextStyle(fontSize: 20, fontFamily: "Cardo")),
              Text("Contoh cardo 219 ( old Style)",
                  style: TextStyle(
                      fontSize: 20,
                      fontFamily: "Cardo",
                      fontFeatures: [FontFeature.oldstyleFigures()])),
              Text("Contoh gabriola 219 ( standart)",
                  style: TextStyle(
                    fontSize: 30,
                    fontFamily: "Gabriola",
                  )),
              Text("Contoh gabriola 219 ( syliset no 5)",
                  style: TextStyle(
                      fontSize: 30,
                      fontFamily: "Gabriola",
                      fontFeatures: [FontFeature.stylisticSet(5)])),
            ],
          ),
        ),
      ),
    );
  }
}
