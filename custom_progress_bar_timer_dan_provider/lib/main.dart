import 'package:custom_progress_bar_timer_dan_provider/Custom%20Progress%20Bar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'dart:async';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Custom Progress Bar"),
        ),
        body: Center(
          child: ChangeNotifierProvider<TimeState>(
            create: (context) => TimeState(),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Consumer<TimeState>(
                  builder: (context, timeState, _) => CustomProgressBar(
                    width: 200,
                    value: timeState.time, // valuenya mengikuti timeStatenya
                    totalValue: 15,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Consumer<TimeState>(
                    builder: (context, timeState, _) => RaisedButton(
                        child: Text(
                          "Start",
                          style: TextStyle(color: Colors.white),
                        ),
                        color: Colors.lightBlue,
                        onPressed: () {
                          Timer.periodic(Duration(seconds: 1), (timer) {
                            if (timeState.time == 0)
                              timer.cancel();
                            else // kalau time statenya udah 0 maka time statenya di matikan kalau tidak makan masih dikurangi 1
                              timeState.time -= 1;
                          });
                        })),
                SizedBox(
                  height: 10,
                ),
                Consumer<TimeState>(
                    builder: (context, timeState, _) => RaisedButton(
                        child: Text(
                          "Start 1",
                          style: TextStyle(color: Colors.white),
                        ),
                        color: Colors.lightBlue,
                        onPressed: () {
                          Timer.periodic(Duration(seconds: 1), (timer) {
                            if (timeState.time == 15)
                              timer.cancel();
                            else // kalau time statenya udah 0 maka time statenya di matikan kalau tidak makan masih dikurangi 1
                              timeState.time += 1;
                          }); 
                        }))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
