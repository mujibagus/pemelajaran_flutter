import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:qrscan/qrscan.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String data = "QR Code Data";

  @override
  Widget build(BuildContext context) {
    
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("QR Scan"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Text(data,
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600)),
              RaisedButton(
                child: Text("Scan QR"),
                onPressed: () {
                  scanQR();
                },
              )
            ],
          ),
        ),
      ),
    );
  }

  void scanQR() async {
    bool result = await PermissionHandler().shouldShowRequestPermissionRationale(PermissionGroup.camera);
    PermissionStatus status = PermissionStatus.granted;
    if (!result)
      status = await PermissionHandler().checkPermissionStatus(PermissionGroup.camera);

    if (result || status == PermissionStatus.granted) {
      String scanResult = await scan();
      setState(() {
        data = scanResult;
      });
    }
  }
}
