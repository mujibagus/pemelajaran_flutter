import 'dart:async';

import 'package:flutter/material.dart';

enum ColorEvent { to_amber, to_blue }

class ColorVloc {
  Color _color = Colors.amber;

  StreamController<ColorEvent> _eventController =
      StreamController<ColorEvent>();
  StreamSink<ColorEvent> get eventSink => _eventController.sink;

  StreamController<Color> _stateControler = StreamController<Color>();
  StreamSink<Color> get _stateSink => _stateControler.sink;
  Stream<Color> get stateStream => _stateControler.stream;

  void _mapEventToState(ColorEvent colorEvent) {
    if (colorEvent == ColorEvent.to_amber)
      _color = Colors.amber;
    else
      _color = Colors.blue;

    _stateSink.add(_color);
  }

  ColorVloc() {
    _eventController.stream.listen(_mapEventToState);
  }

  void dispose() {
    _eventController.close();
    _stateControler.close();
  }
}
