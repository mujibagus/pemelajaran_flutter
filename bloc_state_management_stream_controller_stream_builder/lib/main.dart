import 'package:bloc_state_management_stream_controller_stream_builder/color_vloc.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  ColorVloc vloc = ColorVloc();
  @override
  void dispose() {
    vloc.dispose;
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    

    return MaterialApp(
        home: Scaffold(
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          //  FloatingActionButton(onPressed: (){},) // dikosongin
          FloatingActionButton(
              backgroundColor: Colors.amber,
              onPressed: () {
                vloc.eventSink.add(ColorEvent.to_amber);
              }),
          SizedBox(
            width: 10,
          ),
          FloatingActionButton(
              backgroundColor: Colors.blue,
              onPressed: () {
                vloc.eventSink.add(ColorEvent.to_blue);
              }),
        ],
      ),
      appBar: AppBar(title: Text("VloC tanpa Library")),
      body: Center(
        child: StreamBuilder(
          stream: vloc.stateStream,
          initialData: Colors.amber,
          builder: (context, snapshot) {
            return AnimatedContainer(
              width: 100,
              height: 100,
              color: snapshot.data,
              duration: Duration(milliseconds: 500),
            );
          },
        ),
      ),
    ));
  }
}
