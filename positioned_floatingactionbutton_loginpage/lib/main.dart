import 'package:flutter/material.dart';
import 'package:hero_dan_clipr_rect_widget/MainPage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,// cara menghilangkan mode debug di pojok kanan atas
      home: MainPage(),
    );
  }
}
