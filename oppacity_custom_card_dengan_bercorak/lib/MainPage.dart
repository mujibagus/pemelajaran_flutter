import 'package:flutter/material.dart';

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Custom Card Example",
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Color(0xFF8C062F),
      ),
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
              colors: [Color(0xFFFE5788), Color(0xFFF56D5D)],
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
            )),
          ),
          Center(
            child: SizedBox(
              width: MediaQuery.of(context).size.width *
                  0.8, // lebar chard 90% dari lebar layar menggunakan media Query
              height: MediaQuery.of(context).size.height *
                  0.7, // lebar chard 70% dari lebar layar menggunakan media Query
              child: Card(
                elevation: 10,
                child: Stack(
                  children: <Widget>[
                    Opacity(
                      opacity: 0.7,
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            image: DecorationImage(
                                image: NetworkImage(
                                    "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ8Npl_eHAAn_o5B0k_x-koVWSAftB_kg51kNKfSD9qg7RMtNcu"),
                                fit: BoxFit.cover)),
                      ),
                    ), // untuk membikin transparan pake opacity
                    Container(
                      height: MediaQuery.of(context).size.height * 0.35,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(5),
                            topRight: Radius.circular(5),
                          ), // untuk membuat agar tidak lancip bagian ujung atas saja
                          image: DecorationImage(
                              image: NetworkImage(
                                  "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQl7VG1yAAWATK3R8iLJ-TJVxAHqH3-NlDJZwcHzq2fTeQMeXeE"),
                              fit: BoxFit.cover)),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(
                          20,
                          50 + MediaQuery.of(context).size.height * 0.35,
                          20,
                          20),
                      child: Center(
                        child: Column(
                          children: <Widget>[
                            Text(
                              "Beautifull Sunset latihan oyii",
                              maxLines: 2,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Color(0xFFF56D5D), fontSize: 25),
                            ),
                            Container(
                              margin: EdgeInsets.fromLTRB(0, 20, 0, 15),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    "Posted on",
                                    
                                    
                                    style: TextStyle(
                                        color: Colors.grey, fontSize: 12),
                                  ),
                                  Text(
                                    "18 January 2020",
                                    
                                    style: TextStyle(
                                        color: Colors.blue, fontSize: 12),
                                  ),
                                ],
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                Spacer(flex: 10,),
                                Icon(Icons.thumb_up, size: 18, color: Colors.grey),//icon 
                                Spacer(flex: 1,),
                                Text("99", style: TextStyle(color: Colors.grey)),//text
                                Spacer(flex: 5,),
                                Icon(Icons.comment, size: 18, color: Colors.grey),//icon 
                                Spacer(flex: 1,),//Icon
                                Spacer(flex: 1,),
                                Text("888", style: TextStyle(color: Colors.grey)),//text
                                Spacer(flex: 5,),//text
                                Spacer(flex: 10,),
                              ],
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
