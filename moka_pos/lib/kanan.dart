import 'package:flutter/material.dart';

class Kanan extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.4,
      height: MediaQuery.of(context).size.height * 0.9,
      color: Colors.blue,
      child: Row(crossAxisAlignment: CrossAxisAlignment.start,
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width*0.06,
            height: 50,
            color: Colors.grey[500],
            child:Icon(Icons.account_circle,size: 40,color: Colors.white,) ,
          ),
          Container(
            width: MediaQuery.of(context).size.width*0.2800000000001,
            height: 50,
            color: Colors.grey[400],
          ),
          Container(
            width: MediaQuery.of(context).size.width*0.06,
            height: 50,
            color: Colors.grey[500],
            child: Icon(Icons.apps,size: 40,color: Colors.white,) ,
          )
        ],
      ),
    );
  }
}
