import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:moka_pos/product_card.dart';
import 'package:flutter/services.dart';


class Jard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ProductState>(
      create: (context) => ProductState(),
      child: Container(
        margin: EdgeInsets.only(top: 10,),
        child: Align(
          alignment: Alignment.topCenter,
          child: Consumer<ProductState>(
            builder: (context, productState, _) => ProductCard(
              imageURL:
                  "https://cdn.pixabay.com/photo/2016/02/25/17/08/fruit-1222488_960_720.png",
              name: "Buah Segar 1 Kg",
              quantity: productState.quantity,
              notification:
                  (productState.quantity > 5) ? "Diskon 10%" : null,
              onAddCartTap: () {},
              onIncTap: () {
                productState.quantity++;
              },
              price: "Rp. 25.000",
              
              onDecTap: () {
                if (productState.quantity == 0)
                  productState.quantity =
                      0; // jika quantitynya bernilai 0, maka quantitynya akan terus bernilai 0 jika tidak quantity akan terus dikurangi
                else
                  productState.quantity--;
              },
            ),
          ),
        ),
      ),
    );
  }
}

class ProductState with ChangeNotifier {
  int _quantity = 0;
  int get quantity => _quantity;
  set quantity(int newValue) {
    _quantity = newValue;
    notifyListeners();
  }
}