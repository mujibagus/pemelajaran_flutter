import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:moka_pos/lebar.dart';
import 'package:provider/provider.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
    ]);

    return MaterialApp(debugShowCheckedModeBanner: false, home: Scaffold(body: Lebar(),));
  }
}
