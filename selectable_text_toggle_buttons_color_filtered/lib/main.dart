import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List<bool> isSelected = [true, false, false];
  ColorFilter colorFilter = ColorFilter.mode(Colors.blue, BlendMode.screen);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: ColorFiltered(
        colorFilter: colorFilter,
        child: Scaffold(
          appBar: AppBar(
            title: Text(" Widget Demo GDG 2019 Cina"),
          ),
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: SelectableText(
                    "ini adalah selecttable text. silahkan dipilih",
                    style: TextStyle(fontSize: 20),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                ToggleButtons(
                  children: <Widget>[
                    Icon(Icons.access_alarm),
                    Icon(Icons.ac_unit),
                    Icon(Icons.access_time)
                  ],
                  isSelected: isSelected,
                  onPressed: (index) {
                    setState(() {
                      if (index == 0)
                        colorFilter =
                            ColorFilter.mode(Colors.red, BlendMode.screen);
                      else if (index == 1)
                        colorFilter =
                            ColorFilter.mode(Colors.black, BlendMode.softLight);
                      else
                        colorFilter =
                            ColorFilter.mode(Colors.purple, BlendMode.multiply);

                      ///untuk memilih hanya 1 saja
                      for (int i = 0; i < isSelected.length; i++)
                        isSelected[i] = (i == index)
                            ? true
                            : false; // dimana isSelected yang ke i diganti kalau i = index maka di isi dengan true yang lain di isi false
                      ///untuk multiple choise
                      // isSelected[index] = !isSelected[
                      //     index]; // dimana isSelected yang ke index akan diubah statusnya menjadi lawannya yaitu tidak isSeledted
                    });
                  },
                  fillColor: Colors.red[50],
                  selectedColor: Colors.green,
                  splashColor: Colors.blue,
                  highlightColor: Colors.yellow,
                  borderRadius: BorderRadius.circular(15),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
