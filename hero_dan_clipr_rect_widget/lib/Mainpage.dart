import 'package:flutter/material.dart';
import 'package:hero_dan_clipr_rect_widget/SecondPage.dart';

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightBlue[100],
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text(
          "Latihan Hero Animation",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: GestureDetector(
        onTap: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return SecondPage();
          }));
        },
        child: Hero(
          tag: 'Profile',
          child: ClipRRect(
            // memotong gambar sesuai dengan border radiusnya
            borderRadius: BorderRadius.circular(50),
            child: Container(
              width: 100,
              height: 100,
              child: Image(
                fit: BoxFit.cover,
                image: NetworkImage(
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQaAq2KVrWRNKNhim5BvpBas1CHVIQ6-I9Ie1kKDFx0BHBY9pmm"),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
