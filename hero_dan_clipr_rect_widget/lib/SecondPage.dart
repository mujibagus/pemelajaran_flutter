import 'package:flutter/material.dart';

class SecondPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.pink[100],
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text(
          "Latihan Hero Animation",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Center(
        child: Hero(
          tag: 'Profile',
                  child: ClipRRect(// memotong gambar sesuai dengan border radiusnya
              borderRadius: BorderRadius.circular(100),
              child: Container(
                width: 200,
                height: 200,
                child: Image(
          fit: BoxFit.cover,
          image: NetworkImage(
              "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQaAq2KVrWRNKNhim5BvpBas1CHVIQ6-I9Ie1kKDFx0BHBY9pmm"),
                ),
              ),
            ),
        ),
      ),
    );
  }
}