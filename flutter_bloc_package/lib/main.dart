import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_package/color_bloc.dart';
import 'color_bloc.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: BlocProvider<ColorBloc>(
          create: (BuildContext context) => ColorBloc(), child: MainPage()),
    );
  }
}

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ColorBloc bloc = BlocProvider.of<ColorBloc>(context);
    return Scaffold(
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          FloatingActionButton(
            backgroundColor: Colors.amber,
            onPressed: () {
              
              bloc.add(ColorEvent.to_amber);// awalnya dispatch sekarang jadi add
            },
          ),
          SizedBox(width: 10),
          FloatingActionButton(
            backgroundColor: Colors.blue,
            onPressed: () {
              bloc.add(ColorEvent.to_blue); // awalnya dispatch sekarang jadi add
            },
          )
        ],
      ),
      appBar: AppBar(
        title: Text("BLoc dengan flutter_Bloc"),
      ),
      body: Center(
        child: BlocBuilder<ColorBloc, Color>(
          builder: (context, currentColor) => AnimatedContainer(
              width: 100,
              height: 100,
              color: currentColor,
              duration: Duration(microseconds: 500)),
        ),
      ),
    );
  }
}
