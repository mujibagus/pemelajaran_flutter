import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Latihan Stack dan Align"),
        ),
        body: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                Flexible(
                  flex: 1,
                  child: Row(
                    children: <Widget>[
                      Flexible(
                        flex: 1,
                        child: Container(
                          color: Colors.yellow,
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: Container(
                          color: Colors.white,
                        ),
                      )
                    ],
                  ),
                ),
                Flexible(
                  flex: 1,
                  child: Row(
                    children: <Widget>[
                      Flexible(
                        flex: 1,
                        child: Container(
                          color: Colors.blue,
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: Container(
                          color: Colors.green,
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
            ListView(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.all(10),
                        child: Text("text yg ada di tengah",
                            style: TextStyle(fontSize: 30))),
                    Container(
                        margin: EdgeInsets.all(10),
                        child: Text("text yg ada di tengah",
                            style: TextStyle(fontSize: 30))),
                    Container(
                        margin: EdgeInsets.all(10),
                        child: Text("text yg ada di tengah",
                            style: TextStyle(fontSize: 30))),
                    Container(
                        margin: EdgeInsets.all(10),
                        child: Text("text yg ada di tengah",
                            style: TextStyle(fontSize: 30))),
                    Container(
                        margin: EdgeInsets.all(10),
                        child: Text("text yg ada di tengah",
                            style: TextStyle(fontSize: 30))),
                    Container(
                        margin: EdgeInsets.all(10),
                        child: Text("text yg ada di tengah",
                            style: TextStyle(fontSize: 30))),
                    Container(
                        margin: EdgeInsets.all(10),
                        child: Text("text yg ada di tengah",
                            style: TextStyle(fontSize: 30))),
                    Container(
                        margin: EdgeInsets.all(10),
                        child: Text("text yg ada di tengah",
                            style: TextStyle(fontSize: 30))),
                    Container(
                        margin: EdgeInsets.all(10),
                        child: Text("text yg ada di tengah",
                            style: TextStyle(fontSize: 30))),
                    Container(
                        margin: EdgeInsets.all(10),
                        child: Text("text yg ada di tengah",
                            style: TextStyle(fontSize: 30))),
                    Container(
                        margin: EdgeInsets.all(10),
                        child: Text("text yg ada di tengah",
                            style: TextStyle(fontSize: 30))),
                            Container(
                        margin: EdgeInsets.all(10),
                        child: Text("text yg ada di tengah",
                            style: TextStyle(fontSize: 30))),
                    Container(
                        margin: EdgeInsets.all(10),
                        child: Text("text yg ada di tengah",
                            style: TextStyle(fontSize: 30))),
                  ],
                )
              ],
            ),
            Align(
              alignment: Alignment.bottomCenter,
                child: RaisedButton(
              child: Text("My Button"),
              color: Colors.redAccent,
              onPressed: () {},
            ))
          ],
        ),
      ),
    );
  }
}
